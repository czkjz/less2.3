# Less2.3

## Описание задания
Модифицировать пайплайн [Less 2.1](https://gitlab.com/czkjz/less2.1/-/tree/dev "Less 2.1")
1. Добавить джобу build_dev_pr в стейдж build
2. build dev_pr - сборка и пуш ранером с привелегиями ( добавить к тегу имеджа _pr )
3. build_dev - сборка и пуш ранером с пробросом сокета докера
4. В стейдже run не должно быть docker login
5. Добавить джобу build_fail
6. в ней должен быть любой скрипт \ команда которая зафейлит джобу (екзит код != 0)
7. Её фэйл не должен влиять на продолжение выполнение пайплайна.

## Регистрация ранеров

## ранером с привелегиями 

```yaml
sudo gitlab-runner register -n \
 --url https://gitlab.com/ \
 --registration-token REGISTRATION_TOKEN \
 --executor docker \
 --docker-image "dockcer:latest" \
 --description "Docker runner privileged" \
 --docker-privileged \
 --tag-list docker_runner_pr
```

## ранером с пробросом сокета докера

```yaml
sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token REGISTRATION_TOKEN \
  --executor docker \
  --description "Docker Runner socket" \
  --docker-image "docker:19.03.12" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
  --tag-list docker_runner_socket
  ```

  Ветка [dev](https://gitlab.com/czkjz/less2.3/-/tree/dev "dev") 
